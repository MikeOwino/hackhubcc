// import Image from 'next/image''
import Footer from '../../../components/footer'
import Nav from '../../../components/nav'
import Image from 'next/image'

export default function Home() {
    return (
        <div>
            <Nav />
            <main data-theme="garden">
                <div className="hero min-h-screen bg-base-200">
                    <br />
                    <div className="drawer" data-theme="acid">
                        <input id="my-drawer" type="checkbox" className="drawer-toggle" />
                        <div className="drawer-content">
                            <label htmlFor="my-drawer" className="btn btn-primary drawer-button">Open drawer</label>
                        </div>
                        <div className="drawer-side">
                            <label htmlFor="my-drawer" className="drawer-overlay"></label>
                            <ul className="menu p-4 w-80 bg-base-100 text-base-content">
                                <li><a>Settings</a></li>
                                <li><a>Profile</a></li>
                                <li><a>Log out</a></li>
                            </ul>
                        </div>
                    </div>
                    {/* <div className="hero-content text-centre">
                        
                    </div> */}
                </div>
            </main>
            <Footer />
        </div>
    )
}
