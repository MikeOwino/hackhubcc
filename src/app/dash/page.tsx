// import Image from 'next/image'
import Footer from "../footer";
import Nav from "../nav";
import Link from "next/link";
// import Image from 'next/image'

export default function Home() {
  return (
    <div>
      <Nav />
      <main data-theme="garden">
        <div className="p-4">
          <div className="flex justify-between items-center mb-4">
            <h1 className="text-2xl font-bold">Dashboard</h1>
            <div className="space-x-3">
              <Link href="/">
                <button className="btn btn-info">Logout</button>
              </Link>
              <Link href="/dash/settings">
                <button className="btn btn-info">Settings</button>
              </Link>
            </div>
          </div>

          <div className="grid grid-cols-3 gap-4">
            <div className="p-4 bg-white rounded-lg shadow">
              <h2 className="text-lg font-bold mb-2">Total Users</h2>
              <p className="text-gray-500">1,234</p>
            </div>
            <div className="p-4 bg-white rounded-lg shadow">
              <h2 className="text-lg font-bold mb-2">Total Users</h2>
              <p className="text-gray-500">1,234</p>
            </div>
            <div className="p-4 bg-white rounded-lg shadow">
              <h2 className="text-lg font-bold mb-2">Total Users</h2>
              <p className="text-gray-500">1,234</p>
            </div>
            <div className="p-4 bg-white rounded-lg shadow">
              <h2 className="text-lg font-bold mb-2">Total Users</h2>
              <p className="text-gray-500">1,234</p>
            </div>
            <div className="p-4 bg-white rounded-lg shadow">
              <h2 className="text-lg font-bold mb-2">Total Users</h2>
              <p className="text-gray-500">1,234</p>
            </div>
            <div className="p-4 bg-white rounded-lg shadow">
              <h2 className="text-lg font-bold mb-2">Total Users</h2>
              <p className="text-gray-500">1,234</p>
            </div>
            <div className="p-4 bg-white rounded-lg shadow">
              <h2 className="text-lg font-bold mb-2">Total Users</h2>
              <p className="text-gray-500">1,234</p>
            </div>
            <div className="p-4 bg-white rounded-lg shadow">
              <h2 className="text-lg font-bold mb-2">Total Users</h2>
              <p className="text-gray-500">1,234</p>
            </div>
            <div className="p-4 bg-white rounded-lg shadow">
              <h2 className="text-lg font-bold mb-2">Total Users</h2>
              <p className="text-gray-500">1,234</p>
            </div>

            <div className="p-4 bg-white rounded-lg shadow">
              <h2 className="text-lg font-bold mb-2">Total Sales</h2>
              <p className="text-gray-500">$10,000</p>
            </div>

            <div className="p-4 bg-white rounded-lg shadow">
              <h2 className="text-lg font-bold mb-2">Total Orders</h2>
              <p className="text-gray-500">567</p>
            </div>

            <div className="p-4 bg-white rounded-lg shadow">
              <h2 className="text-lg font-bold mb-2">Pending Tasks</h2>
              <p className="text-gray-500">3</p>
            </div>

            <div className="p-4 bg-white rounded-lg shadow">
              <h2 className="text-lg font-bold mb-2">Total Orders</h2>
              <p className="text-gray-500">567</p>
            </div>

            <div className="p-4 bg-white rounded-lg shadow">
              <h2 className="text-lg font-bold mb-2">Pending Tasks</h2>
              <p className="text-gray-500">3</p>
            </div>

            <div className="p-4 bg-white rounded-lg shadow">
              <h2 className="text-lg font-bold mb-2">Total Orders</h2>
              <p className="text-gray-500">567</p>
            </div>

            <div className="p-4 bg-white rounded-lg shadow">
              <h2 className="text-lg font-bold mb-2">Pending Tasks</h2>
              <p className="text-gray-500">3</p>
            </div>
          </div>
        </div>
      </main>
      <Footer />
    </div>
  );
}
