// import Image from 'next/image'
import Footer from '../footer'
import Nav from '../nav'
import Image from 'next/image'

export default function Home() {
    return (
        <div>
            <Nav />
            <main data-theme="acid">
                <div className="hero min-h-screen bg-base-200">
                    <div className="hero-content flex-col lg:flex-row">
                        <Image src="https://daisyui.com/images/stock/photo-1635805737707-575885ab0820.jpg" className="max-w-sm rounded-lg shadow-2xl" width={200} height={280} alt="Daisy" />
                        <div>
                            <h1 className="text-5xl font-bold">Box Office News!</h1>
                            <p className="py-6">Provident cupiditate voluptatem et in. Quaerat fugiat ut assumenda excepturi exercitationem quasi. In deleniti eaque aut repudiandae et a id nisi.</p>
                            <button className="btn btn-primary">Get Started</button>
                        </div>
                    </div>
                </div>
            </main>
            <Footer />
        </div>
    )
}
