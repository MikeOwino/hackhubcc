import Image from 'next/image'
import Footer from './footer'
import Nav from './nav'
import Link from 'next/link'

export default function Home() {
  return (
    <div>
      <Nav />
      <main data-theme="acid">
        <div className="hero min-h-screen" style={{ backgroundImage: `url("https://daisyui.com/images/stock/photo-1507358522600-9f71e620c44e.jpg")` }}>
          <div className="hero-overlay bg-opacity-60"></div>
          <div className="hero-content text-center text-neutral-content">
            <div className="max-w-md">
              <h1 className="mb-5 text-5xl font-bold">Hello there</h1>
              <p className="mb-5">The React Framework for the Web</p>
              <Link href="/dash"><button className="btn btn-primary"> Get Started</button></Link>
            </div>
          </div>
        </div>
      </main>
      <Footer />
    </div>
  )
}
