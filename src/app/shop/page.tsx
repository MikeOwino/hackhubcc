// import Image from 'next/image'
import Footer from '../footer'
import Nav from '../nav'
import Image from 'next/image'
import Head from 'next/head'

export default function Home() {
    return (
        <div>
            <Head>
                <title>Shop</title>
            </Head>
            <Nav />
            <main data-theme="dracula">
                <br />
                <br />
                <div data-theme="garden" className="card card-compact w-96 bg-base-100 shadow-xl">
                    <figure><Image src="https://daisyui.com/images/stock/photo-1606107557195-0e29a4b5b4aa.jpg" alt="Shoes" width={384} height={227} /></figure>
                    <div className="card-body">
                        <h2 className="card-title">Shoes!</h2>
                        <p>If a dog chews shoes whose shoes does he choose?</p>
                        <div className="card-actions justify-end">
                            <button className="btn btn-primary">Buy Now</button>
                        </div>
                    </div>
                </div>
                <br />
                <div data-theme="garden" className="card card-compact w-96 bg-base-100 shadow-xl">
                    <figure><Image src="https://daisyui.com/images/stock/photo-1606107557195-0e29a4b5b4aa.jpg" alt="Shoes" width={384} height={227} /></figure>
                    <div className="card-body">
                        <h2 className="card-title">Shoes!</h2>
                        <p>If a dog chews shoes whose shoes does he choose?</p>
                        <div className="card-actions justify-end">
                            <button className="btn btn-primary">Buy Now</button>
                        </div>
                    </div>
                </div>
                <br />
                <br />
            </main>
            <Footer />
        </div>
    )
}
